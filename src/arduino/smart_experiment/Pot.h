#ifndef __POT__
#define __POT__

class Potentiometer {

public:
	Potentiometer(int pin);

	int getValue();
	int getLastValue();
	void readValue();

private:
	int pin;
	volatile int lastReadValue;
};

#endif
