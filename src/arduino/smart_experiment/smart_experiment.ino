// SmartExperiment
// Svolto da:
// Angelo Tinti
// Tommaso Bailetti

#include "default_value.h"

#include "Scheduler.h"
#include "MsgService.h"

#include "ProximitySensor.h"
#include "Pot.h"
#include "Button.h"
#include "ButtonImpl.h"
#include "Sonar.h"
#include "servo_motor_impl.h"
#include "TemperatureSensor.h"
#include "Termistor.h"

#include "InitialTask.h"
#include "ExperimentTask.h"
#include "EndTask.h"
#include "StateTurn.h"

Scheduler sched;

void setup() {
	MsgService.init();

	Potentiometer* pot = new Potentiometer(POTENTIOMETER);
	Button* buttonStart = new ButtonImpl(BUTTON_START);
	Led* ledGreen = new Led(LED_GREEN);

	Button* buttonEnd = new ButtonImpl(BUTTON_END);
	ProximitySensor* sonar = new Sonar(SONAR_ECHO, SONAR_TRIG);
	TemperatureSensor* tempSensor = new Termistor(TERMISTOR);
	ServoMotor* motor = new ServoMotorImpl(SERVO_MOTOR);

	Led* ledRed = new Led(LED_RED);

	Serial.begin(115200);
	sched.init(20);

	Task* initialTask = new InitialTask(pot, buttonStart, ledGreen);
	initialTask->init(60);

	Task* experimentTask = new ExperimentTask(sonar, buttonEnd, motor, ledRed, pot, tempSensor);
	experimentTask->init(20);

	Task* endTask = new EndTask(ledRed);
	endTask->init(BLINK_FREQUENCY);

	sched.addTask(initialTask);
	sched.addTask(experimentTask);
	sched.addTask(endTask);
}

void loop() {
	sched.schedule();
}