#ifndef __DEFAULT_VALUE__
#define __DEFAULT_VALUE__

/// Milliseconds time to pass to power down mode
#define SLEEP_TIME 5000
#define ERROR_TIME 2000
#define MAX_TIME 20000
///Centimeters max distant of an object to start experiment
#define MAX_DISTANT 90
#define BLINK_FREQUENCY 160

#define POTENTIOMETER A0
#define TERMISTOR A1

#define BUTTON_START 3
#define BUTTON_END 4

#define LED_GREEN 10
#define LED_RED 9

#define SONAR_ECHO 7
#define SONAR_TRIG 8

#define SERVO_MOTOR 5


#endif 
