#ifndef __EXPERIMENT__
#define __EXPERIMENT__

#include "Task.h"
#include "Led.h"
#include "Button.h"
#include "ButtonImpl.h"
#include "ProximitySensor.h"
#include "servo_motor.h"
#include "Pot.h"
#include "StateTurn.h"
#include "TemperatureSensor.h"

class ExperimentTask : public Task
{

private:
	ProximitySensor* sonar;
	Button* endExperimentButton;
	ServoMotor* speedIndicator;
	Led* experimentLed;
	Potentiometer* pot;
	TemperatureSensor* termometer;
	
	bool first;
	int period;
	int samplingRate;
	int timePassedFromMeasure;
	int timePassedFromStart;
	unsigned long int lastTimeFromStart = 0;
	float lastDistance;
	void checkOrChangeTurn();
	void resetTimePassed();
	void endTurn();
public:

	ExperimentTask(ProximitySensor* sonar, Button* button, ServoMotor* motor, Led* experimentLed, Potentiometer* pot, TemperatureSensor* termometer);
	void init(int period);
	void tick();
};

#endif 

