#ifndef __PROXIMITYSENSOR__
#define __PROXIMITYSENSOR__

class ProximitySensor {

public:
  virtual float getDistance(float environmentTemperature) = 0;
  
};


#endif

