#include "InitialTask.h"
#include "default_value.h"
#include "Arduino.h"
#include "MsgService.h"
#include <avr/sleep.h>


void wakeUp() {}

InitialTask::InitialTask(Potentiometer* pot, Button* button, Led* ledReady) {
	this->pot = pot;
	this->buttonStart = button;
	this->ledReady = ledReady;
}

void InitialTask::init(int period) {
	Task::init(period);
	this->period = period;
	this->timePassed = 0;
	attachInterrupt(digitalPinToInterrupt(2), wakeUp, RISING);
}

void InitialTask::tick() {
	if (StateTurn::getInstance()->isInitialTurn()) {
		MsgService.sendMsg("st:re");
		this->ledReady->switchOn();
		this->pot->readValue();
		
		MsgService.sendMsg("hz:" + String(this->pot->getLastValue()));
		this->timePassed += this->period;
		Serial.println(this->timePassed);
		if (this->timePassed >= SLEEP_TIME)
		{
			this->resetTimePassed();
			MsgService.sendMsg("st:sl");
			this->ledReady->switchOff();
			delay(10);
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);
			sleep_enable();
			sleep_mode();

			sleep_disable();
			this->ledReady->switchOn();
		}
		
		if (this->buttonStart->isPressed())
		{	
			this->resetTimePassed();
			this->ledReady->switchOff();
			StateTurn::getInstance()->setExperimentTurn();
		}
	}
}

void InitialTask::resetTimePassed() {
	this->timePassed = 0;
}
