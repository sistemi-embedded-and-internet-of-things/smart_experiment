#include "ExperimentTask.h"
#include "default_value.h"
#include "Arduino.h"
#include "MsgService.h"

ExperimentTask::ExperimentTask(ProximitySensor* sonar, Button* button, ServoMotor* motor, Led* experimentLed, Potentiometer* pot, TemperatureSensor* termometer)
{
	this->sonar = sonar;
	this->endExperimentButton = button;
	this->speedIndicator = motor;
	this->experimentLed = experimentLed;
	this->pot = pot;
	this->termometer = termometer;
}

void ExperimentTask::resetTimePassed()
{
	this->timePassedFromMeasure = 0;
	this->timePassedFromStart = 0;
}


void ExperimentTask::init(int period)
{
	Task::init(period);
	this->period = period;
	this->timePassedFromStart = 0;
	this->lastDistance = 0;
	this->first = true;
	this->lastTimeFromStart = 0;
}

void ExperimentTask::tick()
{
	if (StateTurn::getInstance()->isExperimentTurn())
	{

		this->checkOrChangeTurn();


		this->samplingRate = (51 - this->pot->getLastValue()) * this->period;
		this->timePassedFromMeasure += this->period;


		if (this->timePassedFromMeasure >= this->samplingRate)
		{
			float temperature = this->termometer->getTemperature();
			float currentDistance = this->sonar->getDistance(temperature);
			float currentSpeed = (this->lastDistance - currentDistance) / (this->samplingRate/static_cast<float>(1000));
			this->timePassedFromMeasure = 0;
			this->lastDistance = currentDistance;

			currentSpeed = currentSpeed > 0 ? currentSpeed : -currentSpeed;

			int pos = currentSpeed * 100 >= 100 ? 100 : currentSpeed * 100;
			pos += 50;
			MsgService.sendMsg("da:" + String(currentDistance*1000));
			this->speedIndicator->setPosition(pos);
		}

	}


}

void ExperimentTask::checkOrChangeTurn()
{
	if (this->endExperimentButton->isPressed() || this->timePassedFromStart >= MAX_TIME)
	{
		this->endTurn();
		StateTurn::getInstance()->setWaitTurn();
	}
	else if (this->first)	
	{
		this->first = false;
		float temperature = this->termometer->getTemperature();
		this->lastDistance = this->sonar->getDistance(temperature) * 100;

		if (this->lastDistance > MAX_DISTANT)
		{
			
			this->endTurn();
			StateTurn::getInstance()->setErrorTurn();
		}
		else
		{
			this->speedIndicator->on();
			this->experimentLed->switchOn();
			this->lastTimeFromStart = millis();
			MsgService.sendMsg("st:me");
		}

	}
	else
	{
		unsigned long int currentMillis = millis();
		int val = currentMillis - this->lastTimeFromStart;
		this->timePassedFromStart += val;
		this->timePassedFromMeasure += val;
		this->lastTimeFromStart = currentMillis;
	}
}

void ExperimentTask::endTurn()
{
	this->experimentLed->switchOff();
	this->resetTimePassed();
	this->speedIndicator->off();
	this->first = true;
}