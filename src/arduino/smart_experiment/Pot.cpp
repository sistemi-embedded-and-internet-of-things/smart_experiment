#include "Pot.h"
#include "Arduino.h"

Potentiometer::Potentiometer(int pin) {
	this->pin = pin;
	this->lastReadValue = 0;
}

int Potentiometer::getValue() {
	this->readValue();
	return this->lastReadValue;
}

int Potentiometer::getLastValue()
{
	return this->lastReadValue;
}

void Potentiometer::readValue()
{
	int read = analogRead(pin);
	float val = read >= 1023 ? read + 20.40 : read + 20.46;
	val = (int)(val / 20.46);
	val = 51 - val;
	this->lastReadValue = val;
}
