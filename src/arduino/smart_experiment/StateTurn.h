#ifndef __STATE_TURN__
#define __STATE_TURN__

class StateTurn {
private:
	bool initialTurn;
	bool experimentTurn;
	bool errorTurn;
	bool waitTurn;

	StateTurn();
	static StateTurn* instance;
	void allTurnOff();

public:
	static StateTurn* getInstance();
	bool isInitialTurn();
	void setInitialTurn();

	bool isExperimentTurn();
	void setExperimentTurn();

	bool isErrorTurn();
	void setErrorTurn();

	bool isWaitTurn();
	void setWaitTurn();
	
};
#endif 
