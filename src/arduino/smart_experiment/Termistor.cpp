#include "Termistor.h"
#include "Arduino.h"

Termistor::Termistor(int pin)
{
	this->pin = pin;
}

float Termistor::getTemperature()
{
	int value = analogRead(this->pin);

	//Operations to truncate to two decimal numbers
	float val = (int)(((float)value / 23.57) * 100);
	val = (float)val / 100;
	return val;
}
