#ifndef __ERROR_TASK__
#define __ERROT_TASK__

#include "Task.h"
#include "Led.h"

class EndTask : public Task
{

private:
	Led* led;
	int period;
	int timePassed;
	int timePassedFromStart;
	bool receivedOk;

	enum { ON, OFF } light;
	enum { ERROR, RESTART} state;

	void resetTimePassed();
	void checkOrChangeTurn();
	void endTurn();

public:
	EndTask(Led* led);
	void init(int period);
	void tick();

};
#endif 
