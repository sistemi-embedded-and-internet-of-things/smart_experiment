#ifndef __INITIAL_TASK__
#define __INITIAL_TASK__

#include "Task.h"
#include "Button.h"
#include "Pot.h"
#include "StateTurn.h"
#include "Led.h"

class InitialTask : public Task {
private:
	
	Potentiometer* pot;
	Button* buttonStart;

	Led* ledReady;

	long timePassed;
	int period;

	void resetTimePassed();

public:
	
	InitialTask(Potentiometer* pot, Button* button, Led* ledRady);
	void init(int period);

	
	void tick();
};

#endif 
