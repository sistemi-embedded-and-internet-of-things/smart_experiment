#include "EndTask.h"
#include "Arduino.h"
#include "StateTurn.h"
#include "default_value.h"
#include "MsgService.h"


EndTask::EndTask(Led* led)
{
	this->led = led;
	this->timePassed = 0;
	this->timePassedFromStart = 0;
	this->light = this->ON;
	this->receivedOk = false;
}	

void EndTask::init(int period)
{
	Task::init(period);
	this->period = period;
}

void EndTask::tick()
{
	if (StateTurn::getInstance()->isErrorTurn() || StateTurn::getInstance()->isWaitTurn())
	{
		this->state = StateTurn::getInstance()->isErrorTurn() ? ERROR : RESTART;
		if (this->timePassed >= BLINK_FREQUENCY)
		{

			switch (this->light)
			{

			case ON:
			{
				this->led->switchOn();
				this->light = OFF;
				break;
			}

			case OFF:
			{
				this->led->switchOff();
				this->light = ON;
				break;
			}

			default:
				break;
			}

			this->resetTimePassed();
		}
		

		switch (this->state)
		{
		case ERROR:
		{
			this->timePassedFromStart += this->period;
			MsgService.sendMsg("st:er");
			break;
		}
		case RESTART:
		{
			MsgService.sendMsg("st:rs");
			if (MsgService.isMsgAvailable())
			{
				Msg* msg = MsgService.receiveMsg();
				if (msg->getContent() = "next")
				{
					this->receivedOk = true;
				}
				delete msg;
			}
			break;
		}
		default:
			break;
		}
		this->timePassed += this->period;
		this->checkOrChangeTurn();
	}
}

void EndTask::resetTimePassed()
{
	this->timePassed = 0;
}

void EndTask::checkOrChangeTurn()
{
	if (this->timePassedFromStart >= ERROR_TIME || this->receivedOk)
	{
		this->endTurn();
	}
}

void EndTask::endTurn()
{
	this->resetTimePassed();
	this->timePassedFromStart = 0;
	this->led->switchOff();
	this->receivedOk = false;
	StateTurn::getInstance()->setInitialTurn();
}