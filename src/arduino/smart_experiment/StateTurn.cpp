#include "StateTurn.h"

StateTurn* StateTurn::instance = 0;

StateTurn* StateTurn::getInstance() {
	if (StateTurn::instance == 0)
	{
		StateTurn::instance = new StateTurn();
	}

	return StateTurn::instance;
}

StateTurn::StateTurn() {
	this->allTurnOff();
	this->initialTurn = true;
}

void StateTurn::allTurnOff()
{
	this->initialTurn = false;
	this->experimentTurn = false;
	this->errorTurn = false;
	this->waitTurn = false;
}

bool StateTurn::isInitialTurn() {
	return this->initialTurn;
}

void StateTurn::setInitialTurn()
{
	this->allTurnOff();
	this->initialTurn = true;
}

bool StateTurn::isExperimentTurn()
{
	return this->experimentTurn;
}

void StateTurn::setExperimentTurn()
{
	this->allTurnOff();
	this->experimentTurn = true;
}

bool StateTurn::isErrorTurn()
{
	return this->errorTurn;
}

void StateTurn::setErrorTurn()
{
	this->allTurnOff();
	this->errorTurn = true;
}

bool StateTurn::isWaitTurn()
{
	return this->waitTurn;
}

void StateTurn::setWaitTurn()
{	
	this->allTurnOff();
	this->waitTurn = true;
}