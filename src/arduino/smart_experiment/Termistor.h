#ifndef __TERMISTOR__
#define __TERMISTOR__

#include "TemperatureSensor.h"

class Termistor : public TemperatureSensor 
{
public:
	Termistor(int pin);
	float getTemperature();
private:
	int pin;
};

#endif // !__TERMISTOR__
